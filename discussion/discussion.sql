-- ALTER TABLE aalbums RENAME COLUMN data_released TO date_released;

-- INSERT RECORDS (CREATE);

-- INSERT INTO <name_of_table> (columns_on_table) VALUES (values_per_column);
INSERT INTO artists(name) VALUES ('Rivermaya');
INSERT INTO artists(name) VALUES ('Psy');

INSERT INTO albums(album_title, date_released, artist_id) VALUES ('Psy 6', '2012-1-1', 2);
INSERT INTO albums(album_title, date_released, artist_id) VALUES ('Trip', '1996-1-1', 1);


INSERT INTO songs(song_title, song_length, song_genre, album_id) VALUES ('Gangnam Style', 253, 'K-pop', 1);
INSERT INTO songs(song_title, song_length, song_genre, album_id) VALUES ('Kundiman', 234, 'OPM', 2);
INSERT INTO songs(song_title, song_length, song_genre, album_id) VALUES ('Kisapmata', 259, 'OPM', 2);

-- SELECTING RECORDS (RETRIEVE);

-- SELECT * FROM <table_name>;
-- SELECT <column or column_names> FROM <table_name>;
SELECT * FROM songs;
SELECT * FROM albums;
-- General query
SELECT song_title, song_length FROM songs;
-- Conditional query
SELECT * FROM songs WHERE song_genre = 'OPM';
-- Combination
SELECT song_title, song_length FROM songs WHERE song_genre = 'OPM';


-- AND and OR keyword
SELECT * FROM songs WHERE song_length > 240 AND song_genre = 'OPM';
SELECT * FROM songs WHERE song_length > 240 OR song_genre = 'OPM';


-- UPDATING RECORDS (UPDATE)
-- Without where it will update all
-- UPDATE <table_name> SET <column_name> = <value> WHERE <condition>;

-- We used ID as condition to target specific data to update
UPDATE songs SET song_length = 260 WHERE id = 2;
UPDATE songs SET song_length = 300 WHERE song_length > 240 AND song_genre = 'OPM';


-- UPDATE COLUMN table
-- ALTER TABLE <table_name> RENAME COLUMN <column_to_rename> TO <new_column_name>;


-- DELETE RECORDS (DELETE)
-- BEFORE USING THIS MAKE SURE YOU HAVE WHERE FOR CONDITION
-- DELETE FROM <table_name> WHERE <condition>;
DELETE FROM songs WHERE song_genre = 'OPM' AND song_length > 240;



-- Display the table columns and data types of the columns
-- DESCRIBE <table_name>;